clear all;
close all;
clc

%% load modified coefficients
load ll_subband_modified.mat cA
load lh_subband_modified.mat cH
load hl_subband_modified.mat cV
load hh_subband_modified.mat cD

cA = double(cA)
cH = double(cH)
cV = double(cV)
cD = double(cD)


%% apply the inverse integer wavelet transform

LS = liftwave('haar', 'int2int');
els = {'p', [-0.125 0.125], 0};
lsnew = addlift(LS, els);
new_image = ilwt2(cA, cH, cV, cD, lsnew);

%% save the new image
imwrite(new_image, 'watermarked_image.png')