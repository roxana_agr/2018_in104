clear all;
close all;
clc

%% read input image
img = imread('cameraman.png');

%% apply the integer wavelet transform
img = double(img);
LS = liftwave('haar', 'int2int');
els = {'p', [-0.125 0.125], 0};
lsnew = addlift(LS, els);

[cA, cH, cV, cD] = lwt2(img, lsnew);

save ll_subband.mat cA
save lh_subband.mat cH
save hl_subband.mat cV
save hh_subband.mat cD
