#!/usr/bin/env python
"""Python script to create the watermark to be inserted."""

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
# from PIL import Image
# from PIL import ImageDraw
import numpy as np
import os
import sys 

current_path = os.path.dirname(os.path.realpath(__file__))
images_path = os.path.join(current_path, 'images')

# text to be written
text = sys.argv[1]

# set path to input image
image_path = os.path.join(images_path, 'main_logo.png')
# read image
img = mpimg.imread(image_path)
# show image in figure
plt.imshow(img)
# remove axis
plt.axis('off')
# put text at coordinates 100, 400
plt.text(100, 400, text, style='italic')

outpath = os.path.join(images_path, 'new_logo.png')
# save figure
plt.savefig(outpath)