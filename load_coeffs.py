#!/usr/bin/env python
"""Script to load subband coefficients and to visualize the wavelet subbands.
v.1 created on 16/04/2018
@Roxana Agrigoroaie.
"""
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import scipy.io as sio
# from PIL import Image
# from PIL import ImageDraw
import numpy as np
import os
import sys 

# load .mat files
mat_content_hl = sio.loadmat('data/hl_subband.mat')
mat_content_lh = sio.loadmat('data/lh_subband.mat')
mat_content_ll = sio.loadmat('data/ll_subband.mat')
mat_content_hh = sio.loadmat('data/hh_subband.mat')

# read cameraman png file
img = mpimg.imread('images/cameraman.png')
# display the original image
plt.imshow(img, cmap='gray')
plt.show()

# extract the data from the .mat files
hl_subband = mat_content_hl['cV']
lh_subband = mat_content_lh['cH']
ll_subband = mat_content_ll['cA']
hh_subband = mat_content_hh['cD']

# display each subband in a different subplot
fig = plt.figure()
fig.add_subplot(2, 2, 1)
plt.imshow(ll_subband, cmap='gray')
plt.axis('off')

fig.add_subplot(2, 2, 2)
plt.imshow(lh_subband, cmap='gray')
plt.axis('off')

fig.add_subplot(2, 2, 3)
plt.imshow(hl_subband, cmap='gray')
plt.axis('off')

fig.add_subplot(2, 2, 4)
plt.imshow(hh_subband, cmap='gray')

# remove axis
plt.axis('off')
plt.show()

