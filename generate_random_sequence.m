clear all;
close all;
clc

% generate pseudo random numbers with LCG
a = 5;
c = 11;
m = 256;
x = 2;

[p, mean, var, oneperiod] = lcg(a, c, m, x);
vari = oneperiod(1:256);

% order sequence
[ord, index] = sort(vari)

% sort the index to do the inverse permutation

[ord_inv, index_inv] = sort(index)